import os
import arrow
from flask import Flask, redirect, url_for, request, render_template
from pymongo import MongoClient
from acp_times import open_time, close_time

app = Flask(__name__)

client = MongoClient(os.environ['DB_PORT_27017_TCP_ADDR'], 27017)
db = client.tododb


@app.route('/')
def todo():
    return render_template('todo.html')


@app.route('/new', methods=['POST'])
def new():
    distance = request.form['distance']
    brevet = request.form['brevet']
    date = request.form['date']
    time = request.form['time']

    #Store data in MongoDB
    date_time = date+" "+time
    start_time = arrow.get(date_time, "YYYY-M-D HH:mm")
    opentime = open_time(int(distance), int(brevet), start_time)
    closetime = close_time(int(distance), int(brevet), start_time)

    item_doc = {
        'open_time': opentime,
        'close_time': closetime,
    }
    db.tododb.insert_one(item_doc)

    return redirect(url_for('todo'))


@app.route('/display')
def display():
    _items = db.tododb.find()
    items = [item for item in _items]
    message = ""
    if len(items) == 0:
        message = "There is no database. Please entry again"
    return render_template('display.html', items=items,message=message)


if __name__ == "__main__":
    app.run(host='0.0.0.0', debug=True)
